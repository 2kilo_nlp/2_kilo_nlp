import json
import re
from openpyxl import load_workbook

with open(
        r"C:\Users\dchapman\PycharmProjects\MakeTrainingData\replacement_dict.json") as d:
    replacement_dict = json.load(d)


def SimulSub(string_):
    if len(replacement_dict) == 0:
        return string_

    def repl_(regex_):
        match_ = regex_.group()
        for x in (sorted(replacement_dict.keys(), key=lambda x: len(x))):
            # the below line could cause problems for lookahead / lookbehind / beginning / end regex
            if (re.search(x, match_) != None): return replacement_dict[x]
        print("error with SimulSub")
        print("check lookahead/lookbehind/beginning/end regex")
        return match_

    pattern = ''
    for i in sorted(replacement_dict.keys(), key=lambda x: len(x)):
        if i[0] == '(':
            pattern += (i + '|')
        else:
            pattern += ('(' + i + r')\b|')
    pattern = pattern[0:-1]
    return re.sub(pattern, repl_, string_)

wb = load_workbook("Test_cleaned.xlsx")
ws = wb.active
for i in range(1,5325):
    old_prob = ws.cell(row=i, column=9).value
    if old_prob:
        new_prob = SimulSub(old_prob)
        ws.cell(row=i,column = 10, value=new_prob)

wb.save("Test_cleaned.xlsx")



