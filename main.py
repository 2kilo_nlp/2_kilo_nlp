from flask import Flask, render_template, request, redirect, Response, jsonify, url_for
from openpyxl import load_workbook

app = Flask(__name__)
wb = load_workbook("Test_cleaned.xlsx")
ws = wb.active
probCol = 10
activeRow = 34


@app.route('/')
def output():
    # serve index template
    global activeRow
    activeRow += 1
    data = ws.cell(row=activeRow, column=probCol).value
    return render_template('index.html', ProblemDesc=data)


@app.route('/receiver', methods=['POST'])
def worker():
    global activeRow
    global ws
    print('Incoming..')
    jsondata = list(request.form.to_dict().values())
    for i in range(12):
        ws.cell(row=activeRow, column=probCol+i+1, value=jsondata[i])
    wb.save("TestResults.xlsx")
    return redirect("/")

        # GET request

@app.route('/sender', methods=['GET'])
def sender():
    global activeRow
    activeRow += 1
    data = ws.cell(row=activeRow, column=probCol).value
    return data

if __name__ == '__main__':
    # run!
    app.run()
